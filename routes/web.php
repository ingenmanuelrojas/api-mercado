<?php

/*------------------------------------
INICIO RUTAS DE AUTENTICACION 
------------------------------------*/ 
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Email Verification Routes...
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
/*------------------------------------
FIN RUTAS DE AUTENTICACION 
------------------------------------*/ 


Route::get('/home/my-tokens', 'HomeController@getTokens')->name('personalTokens');
Route::get('/home/my-clients', 'HomeController@getClients')->name('personalClients');
Route::get('/home/authorized-clients', 'HomeController@getAuthorizedClients')->name('authorizedClients');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
})->middleware('guest');//GUEST ES PARA QUE SOLO LOS USUARIOS LOGUEADOS PUEDAN ACCEDER