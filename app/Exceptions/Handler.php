<?php

namespace App\Exceptions;

use Exception;
use App\Traits\ApiResponser;
use Illuminate\Database\QueryException;
use Asm89\Stack\CorsService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;


class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception){
        parent::report($exception);
    }

    public function render($request, Exception $exception)
    {
        $response = $this->handleException($request, $exception);
        app(CorsService::class)->addActualRequestHeaders($response, $request);
        return $response;
    }

    public function handleException($request, Exception $exception){
        //VALIDAR EXEPCIONES EN EL INGRESO DE DATOS DEL REQUEST
        if ($exception instanceOf ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        //VALIDAR EXCEPCIONES AL NO CONSEGUIR DATOS EN LAS BUSQUEDAS
        if($exception instanceOf ModelNotFoundException){
            $modelo = strtolower(class_basename($exception->getModel()));
            return $this->errorResponse("No existe ninguna instancia de {$modelo} con el ID especificado",404);
        }

        //VALIDAR EXCEPCIONES DE AUTENTICACION
        if ($exception instanceof AuthenticationException) {
            //Si la excepcion viene desde el login, se redirecciona d enuevo al login
            if ($this->isFrontend($request)) {
                return redirect()->guest('login');
            }else{//Sino quiere decir que es de la API y se retorna un json
                return $this->unauthenticated($request, $exception);
            }
        }

        //VALIDAR EXCEPCIONES PARA PERMISOS A LAS ACCIONES DEL API
        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse("No tiene permisos para ejecutar esta accion", 403);
        }

        //VALIDAR EXCEPCIONES PARA LAS URL INCORRECTAS
        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse("No se encontro la URL especificada", 404);
        }

        //VALIDAR EXCEPCION PARA METODOS NO VALIDOS
        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse("El método espécificado en la peticion no es válido", 405);
        }

        //VALIDAR EXCEPCION GENERAL PARA CUALQUIER PETICIONES HTTP
        if ($exception instanceof HttpException) {
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        //VALIDAR EXCEPCION PARA ERRORES AL ELIMINAR ELEMENTOS QUE TENGAN CLAVE FORANEA
        if ($exception instanceof QueryException) {
            //dd($exception);

            //Obtener el codigo del error de la excepcion
            $codigo = $exception->errorInfo[1];

            if ($codigo == 1451) {
                return $this->errorResponse("No se puede eliminar de forma permanente el recurso porque esta relacionado con algún otro.", 409);
            }
        }

        //VALIDACION PARA LOS ERRORES DE CSRF TOKEN
        if ($exception instanceof TokenMismatchException) {
            return redirect()->back()->withInput($request->input());
        }

        //VALIDAR EN QUE ENTORNO DE DESARROLLO ESTAMOS TRABAJANDO, SI ES EN DESARROLLO MOSTRARA
        //EL ERROR COMPLETO, SINO MOSTRARA UN ERROR 500
        if (config('app.debug')) {
            return parent::render($request, $exception);
        }else{
            //VALIDAR EXCEPCION PARA CUALQUIER FALLA INESPERADA
            return $this->errorResponse("Error interno del servidor", 500);            
        }

    }

    protected function unauthenticated($request, AuthenticationException $exception){
        return $this->errorResponse("No autenticado", 401);

    }

    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();
        //Validar si viene del Frontend la excepcion
        if ($this->isFrontend($request)) {
            return $request->ajax() ? response()->json($errors, 422) : redirect()
                ->back()
                ->withInput($request->input())
                ->withErrors($errors);
        }else{
            return $this->errorResponse($errors, 422);
        }
    }
    
    private function isFrontend($request){
        //Verificar que es una ruta valida de las rutas web del proyecto frontend
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
}
