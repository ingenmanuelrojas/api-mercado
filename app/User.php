<?php

namespace App;

use App\Transformers\UserTransformer;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    const USUARIO_VERIFICADO = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const USUARIO_ADMINISTRADOR = 'true';
    const USUARIO_REGULAR = 'false';

    public $transformer = UserTransformer::class;

    protected $table = "users";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'verification_token',
        'admin',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'verification_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //MUTADOR PARA QUE EL NOMBRE SIEMPRE ESTE EN MINISCULA
    public function setNameAttribute($valor){
        $this->attributes['name'] = strtolower($valor);
    }

    //ACCESOR PARA CONVERTIR LAS PRIMERAS PALABRAS DEL NOMBRE EN MAYUSCULA
    public function getNameAttribute($valor){
        return ucwords($valor);
    }

    //MUTADOR PARA QUE EL CORREO SIEMPRE ESTE EN MINISCULA
    public function setEmailAttribute($valor){
        $this->attributes['email'] = strtolower($valor);
    }

    public function esVerificado(){
        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public function esAdministrador(){
        return $this->admin == User::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificationToken(){
        return str_random(40);
    }
}
