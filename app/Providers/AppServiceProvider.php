<?php

namespace App\Providers;
use App\User;
use App\Product;
use App\Mail\UserCreated;
use App\Mail\UserMailChanged;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //EVENTO AUTOMATICO PARA ENVIAR CORREO AL MOMENTO DE ACTUALIZAR EL CORREO DEL USUARIO
        User::updated(function($user){
            if ($user->isDirty('email')) { //VERIFICAR SI EL VALOR DEL CORREO ELECTRONICO CAMBIO SU VALOR
                //EL METODO RETRY EJECUTA DE NUEVO LOS EVENTOS EN CASO DE ALGUN ERROR
                retry(5, function() use ($user) {
                    Mail::to($user)->send(new UserMailChanged($user));
                }, 100);
            }
        });

        //EVENTO AUTOMATICO PARA ENVIAR CORREO AL MOMENTO DE CREAR UN USUARIO
        User::created(function($user){
            retry(5, function() use ($user) {
                Mail::to($user)->send(new UserCreated($user));//LARAVEL AUTOMATICAMENTE OBTENDRA EL EMAIL DE USUARIO
            }, 100);
        });

        //EVENTO AUTOMATICO PARA COLOCAR EL PRODUCTO NO DISPONIBLE EN CASO DE QUE LA CANTIDAD LLEGUE A CERO
        Product::updated(function($product){
            if ($product->quantity == 0 && $product->estaDisponible()) {
                $product->status = Product::PRODUCTO_NO_DISPONIBLE;
                $product->save();
            }
        });

    }
}
