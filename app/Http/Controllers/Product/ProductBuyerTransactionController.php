<?php

namespace App\Http\Controllers\Product;

use App\User;
use App\Product;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Transformers\TransactionTransformer;
use App\Http\Controllers\ApiController;

class ProductBuyerTransactionController extends ApiController
{

    public function __construct(){
        parent::__construct();
        $this->middleware('transform.input:' . TransactionTransformer::class)->only(['store','update']);
        $this->middleware('scope:purchase-product')->only('store');
    }
    

    public function store(Request $request, Product $product, User $buyer){
        
        $rules = [
            'quantity' => 'required|integer|min:1'
        ];

        $this->validate($request, $rules);

        //Validar si el id del comprador y el vendedor sean diferentes
        if ($buyer->id == $product->seller_id) {
            return $this->errorResponse('El comprador debe ser diferente al vendedor', 409);
        }
        
        //Validar si el comprador  es usuario verificado
        if (!$buyer->esVerificado()) {
            return $this->errorResponse('El comprador debe ser un usuario verificado', 409);
        }
        
        //Validar si el vendedor  es usuario verificado
        if (!$product->seller->esVerificado()) {
            return $this->errorResponse('El vendedor debe ser un usuario verificado', 409);
        }

        //Validar si el producto esta disponible
        if (!$product->estaDisponible()) {
            return $this->errorResponse('El producto para esta transacción no está disponible', 409);
        }

        //Validar diponibilidad de la cantidad de productos solicitados
        if ($product->quantity < $request->quantity) {
            return $this->errorResponse('El producto no tiene la cantidad disponible requerida para esta transacción', 409);
        }

        //Si pasa las validaciones se procede a crear la transaccion
        return DB::transaction(function () use($request, $product, $buyer) {
            $product->quantity -= $request->quantity;
            $product->save();

            $transactions = Transaction::create([
                'quantity'   => $request->quantity,
                'buyer_id'   => $buyer->id,
                'product_id' => $product->id,
            ]);
            return $this->showOne($transactions, 201);
        });
    }
}
