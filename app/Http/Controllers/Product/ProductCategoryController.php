<?php

namespace App\Http\Controllers\Product;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ProductCategoryController extends ApiController
{

    public function __construct(){
        $this->middleware('auth:api')->except(['index']);
        $this->middleware('client.credentials')->only(['index']);
        $this->middleware('scope:manage-product')->except('index');
    }

    public function index(Product $product)
    {
        $categories = $product->categories;
        return $this->showAll($categories);
    }

    //AGREGAR UNA CATEGORIA A UN PRODUCTO
    public function update(Request $request, Product $product, Category $category){
        //Metodo sync = sustituye toda la lista por la nueva que hay
        //Metodo attach = agrega nuevas categorias dejando las anteriores, pero si volvemos a enviar la peticion sigue repitiendo la insercion
        //Metodo syncWithoutDetaching = mantiene la lista anterior y no repite las nuevas inserciones

        $product->categories()->syncWithoutDetaching([$category->id]);
        return $this->showAll($product->categories);
    }

    public function destroy(Product $product, Category $category){
        if (!$product->categories->find($category->id)) {
            return $this->errorResponse('La categoria especificada no es una categoria válida para este producto', 404);
        }

        $product->categories()->detach([$category->id]);
        return $this->showAll($product->categories);
    }
}
