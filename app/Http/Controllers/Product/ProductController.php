<?php

namespace App\Http\Controllers\Product;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class ProductController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only(['index', 'show']);
    }

    public function index()
    {
        $productos = Product::all();
        return $this->showAll($productos);
    }

    public function show(Product $product)
    {
        return $this->showOne($product, 200);
    }
}
