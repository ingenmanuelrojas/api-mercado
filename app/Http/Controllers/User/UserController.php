<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Mail\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Transformers\UserTransformer;
use App\Http\Controllers\ApiController;

class UserController extends ApiController
{

    public function __construct(){
        $this->middleware('client.credentials')->only(['store', 'resend']);
        $this->middleware('auth:api')->except(['store', 'verify', 'resend']);
        $this->middleware('transform.input:' . UserTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:manage-account')->only('show','update');
    }


    public function index(){
        $usuarios = User::all();
        return $this->showAll($usuarios);
    }

    public function store(Request $request){
        //REGLAS DE VALIDACION
        $reglas = [
            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ];

        $this->validate($request,$reglas);

        //OBTENER TODO EL REQUEST ENVIADO
        $campos = $request->all();

        //CREAR REGLAS PARA CAMPOS ESPECIALES
        $campos['password']           = bcrypt($request->password);
        $campos['verified']           = User::USUARIO_NO_VERIFICADO;
        $campos['verification_token'] = User::generarVerificationToken();
        $campos['admin']              = User::USUARIO_REGULAR;

        //CREAR EL USUARIO
        $usuario = User::create($campos);

        //RESPONSE
        return $this->showOne($usuario, 201);
    }

    public function show(User $user){ //INYECCION DE DEPENDENCIA, HACE UNA BUSQUEDA SI EL USUARIO EXISTE
        //$usuario = User::findOrFail($id);

        return $this->showOne($user, 200);
    }

    public function update(Request $request, User $user){
        //Obtener el usuario, sino existe retorna una excepcion
        //$user = User::findOrFail($id);

        //REGLAS DE VALIDACION
        $reglas = [
            'email'    => 'email|unique:users,email,' . $user->id, //VALIDA QUE EL CORREO SEA UNICO EXCEPTUANDO EL EMAIL DEL ID DEL USUARIO ACTUAL
            'password' => 'min:6|confirmed',
            'admin' => 'in:' . User::USUARIO_ADMINISTRADOR . ',' . User::USUARIO_REGULAR,
        ];

        $this->validate($request,$reglas);

        if($request->has('name')){
            $user->name = $request->name;
        }

        if($request->has('email') && $user->email != $request->email){
            $user->verified = User::USUARIO_NO_VERIFICADO;
            $user->verification_token = User::generarVerificationToken();
            $user->email = $request->email;
        }

        if($request->has('password')){
            $user->password = bcrypt($request->name);
        }

        if($request->has('admin')){
            if(!$user->esVerificado()){
                return $this->errorResponse('Unicamente los usuarios verificados pueden cambiar valor de administrador', 409);
            }else{
                $user->admin = $request->admin;
            }
        }

        //VALIDAR QUE LOS VALORES ENVIADOS POR REQUEST HAYAN TENIDO CAMBIOS
        if(!$user->isDirty()){
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }else{
            $user->save();

            return $this->showOne($user, 200);
        }
    }

    public function destroy(User $user){
        //$user = User::findOrFail($id);

        $user->delete();

        return $this->showOne($user, 200);
    }

    public function me(Request $request){
        $user = $request->user();
        return $this->showOne($user, 200); 
    }

    public function verify($token){
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->verified = User::USUARIO_VERIFICADO;
        $user->verification_token = null;

        $user->save();
        return $this->showMessage('La cuenta ha sido verificada', 200);
    }

    public function resend(User $user){
        if ($user->esVerificado()) {
            return $this->errorResponse('Este usuario ya ha sido verificado', 409);
        }else{
            retry(5, function() use ($user) {
                Mail::to($user)->send(new UserCreated($user));//LARAVEL AUTOMATICAMENTE OBTENDRA EL EMAIL DE USUARIO
            }, 100);
            return $this->showMessage('El correo de verificación se ha reenviado', 200);
        }
    }
}
