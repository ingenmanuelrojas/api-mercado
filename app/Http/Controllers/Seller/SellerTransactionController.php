<?php

namespace App\Http\Controllers\Seller;

use App\Seller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class SellerTransactionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:read-general')->only('index');
    }

    public function index(Seller $seller){
        $sellers = $seller->products()
        ->whereHas('transactions') //Obtener los productos que tengan asociada al menos 1 transaction
        ->with('transactions')
        ->get()
        ->pluck('transactions')
        ->collapse();
        
        return $this->showAll($sellers);
    }
}
