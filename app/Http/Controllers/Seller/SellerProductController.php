<?php

namespace App\Http\Controllers\Seller;

use App\Seller;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ApiController;
use App\Transformers\ProductTransformer;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductController extends ApiController
{
    public function __construct(){
        parent::__construct();
        $this->middleware('transform.input:' . ProductTransformer::class)->only(['store','update']);
        $this->middleware('scope:manage-product')->except('index');
        
    }

    public function index(Seller $seller){
        $products = $seller->products;
        return $this->showAll($products);
    }


    public function store(Request $request, User $seller){
        $reglas = [
            'name'       => 'required',
            'description'=> 'required',
            'quantity'   => 'required|integer|min:1',
            'image'      => 'required',
        ];

        $this->validate($request,$reglas);

        //OBTENER TODO EL REQUEST ENVIADO
        $campos = $request->all();
        $campos['status']    = Product::PRODUCTO_NO_DISPONIBLE;
        $campos['image']     = $request->image->store('');
        $campos['seller_id'] = $seller->id;

        $product = Product::create($campos);
        return $this->showOne($product, 201);
    }

    public function update(Request $request, Seller $seller, Product $product){
        $reglas = [
            'image'    => 'image',
            'quantity' => 'integer|min:1',
            'status'   => 'in: '.Product::PRODUCTO_NO_DISPONIBLE.','.Product::PRODUCTO_DISPONIBLE,
        ];

        $this->validate($request,$reglas);

        //Llamar al metodo que verifica si es el vendedor del proudcto
        $this->verificarVendedor($seller, $product);

        $product->fill($request->only([
            'name',
            'description',
            'quantity',
        ]));

        
        
        //Cambiar el estado solo si el producto tiene asociada una categoria
        if ($request->has('status')) {
            $product->status = $request->status;
            
            //Verificar si el producto es disponible y no tiene categorias asignadas, se debe retornar error
            if ($product->estaDisponible() && $product->categories()->count() == 0) {
                return $this->errorResponse('Un producto activo debe tener al menos asignada una categoria', 409);
            }
        }
        
        //Verificar si obtenemos una imagen en el request
        if ($request->hasFile('image')) {
            Storage::delete($product->image);
            $product->image = $request->image->store('');
        }
        
        if ($product->isClean()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }else{
            $product->save();
            return $this->showOne($product, 200);
        }
    }

    public function destroy(Seller $seller, Product $product){
        $this->verificarVendedor($seller, $product);

        Storage::delete($product->image);

        $product->delete();

        return $this->showOne($product, 200);
    }

    public function verificarVendedor(Seller $seller, Product $product){
        if ($seller->id != $product->seller_id) {
            throw new HttpException(422,'El vendedor especificado no es el propietario del producto');
        }
    }
}
