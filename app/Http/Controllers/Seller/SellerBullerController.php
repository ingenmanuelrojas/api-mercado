<?php

namespace App\Http\Controllers\Seller;

use App\Seller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class SellerBullerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }

    public function index(Seller $seller){
        $buyers = $seller->products()
        ->whereHas('transactions') //Obtener los productos que tengan asociada al menos 1 transaction
        ->with('transactions.buyer')
        ->get()
        ->pluck('transactions')//Primera se obtienen solo el collection de transacciones
        ->collapse() //Se juntan todoas en 1 sola lista
        ->pluck('buyer') // Se accede a solo la collection de compradores
        ->unique() 
        ->values();
        
        return $this->showAll($buyers);
    }
}
