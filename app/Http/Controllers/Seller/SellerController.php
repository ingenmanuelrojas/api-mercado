<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Seller;

class SellerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:read-general')->only('show');
    }
    
    public function index()
    {
        $vendedores = Seller::has('products')->get();
        return $this->showAll($vendedores);
    }

    public function show($id)
    {
        $vendedores = Seller::has('products')->findOrFail($id);
        return $this->showOne($vendedores, 201);
    }

}
