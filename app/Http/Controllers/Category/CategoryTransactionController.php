<?php

namespace App\Http\Controllers\Category;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CategoryTransactionController extends ApiController
{
    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }
    

    public function index(Category $category){
        $transactions = $category->products()
        ->whereHas('transactions') //Obtener los productos que tengan asociada al menos 1 transaction
        ->with('transactions')
        ->get()
        ->pluck('transactions')
        ->collapse();
        
        return $this->showAll($transactions);
    }

}
