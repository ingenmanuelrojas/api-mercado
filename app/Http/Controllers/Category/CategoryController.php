<?php

namespace App\Http\Controllers\Category;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\CategoryTransformer;
use App\Http\Controllers\ApiController;

class CategoryController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only(['index','show']);
        $this->middleware('auth:api')->except(['index','show']); //METODO PARA PROTEGER TODAS LAS RUTAS MENOS INDEX Y SHOW
        $this->middleware('transform.input:' . CategoryTransformer::class)->only(['store','update']);

        
    }

    public function index()
    {
        $categoria = Category::all();
        return $this->showAll($categoria);
    }

    public function store(Request $request)
    {
        //REGLAS DE VALIDACION
        $reglas = [
            'name'       => 'required',
            'description'=> 'required',
        ];

        $this->validate($request,$reglas);

        $categoria = Category::create($request->all());
        return $this->showOne($categoria, 201);
    }

    public function show(Category $category)
    {
        return $this->showOne($category);
    }

    public function update(Request $request, Category $category)
    {
        //El metodo fill(), obtiene los valores que se van a actualizar
        //El metodo Only, obtiene unicamente los valores que requerimos, ignorando los valores adicionales
        //que el usuario envie
        $category->fill($request->only([
            'name',
            'description'
        ]));

        //Verificar si los datos recibidos cambiaron en algun valor de su estado original
        if ($category->isClean()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }else{
            $category->save();
            return $this->showOne($category, 200);
        }
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return $this->showOne($category, 200);
    }
}
