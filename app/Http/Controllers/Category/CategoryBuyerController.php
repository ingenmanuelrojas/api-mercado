<?php

namespace App\Http\Controllers\Category;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CategoryBuyerController extends ApiController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(Category $category){
        $buyers = $category->products()
        ->whereHas('transactions') //Obtener los productos que tengan asociada al menos 1 transaction
        ->with('transactions.buyer')
        ->get()
        ->pluck('transactions')//Primera se obtienen solo el collection de transacciones
        ->collapse() //Se juntan todoas en 1 sola lista
        ->pluck('buyer') // Se accede a solo la collection de compradores
        ->unique() 
        ->values();
        
        return $this->showAll($buyers);
    }
}
