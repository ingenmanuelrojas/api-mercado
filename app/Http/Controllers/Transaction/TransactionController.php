<?php

namespace App\Http\Controllers\Transaction;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class TransactionController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:read-general')->only('show');
    }

    public function index()
    {
        $transactions = Transaction::all();
        return $this->showAll($transactions, 200);
    }


    public function show(Transaction $transaction)
    {
        return $this->showOne($transaction, 200);
    }

}
