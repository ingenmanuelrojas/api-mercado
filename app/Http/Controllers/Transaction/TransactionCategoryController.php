<?php

namespace App\Http\Controllers\Transaction;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class TransactionCategoryController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only(['index','show']);
    }

    public function index(Transaction $transaction)
    {
        //Obtener la lista de las categorias de una transaccion especifica
        //La transaccion tiene relacion con los productos y estos con las categorias
        $categories = $transaction->product->categories;

        return $this->showAll($categories,200);
    }

}
