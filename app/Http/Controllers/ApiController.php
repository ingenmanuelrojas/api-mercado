<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    use ApiResponser;

    public function __construct(){
        //Llamar al midleware para proteger las rutas
        $this->middleware('auth:api');
    }

}
