<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerSellerController extends ApiController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(Buyer $buyer){
        $sellers = $buyer->transactions()->with('product.seller') //Le decimos que nostraiga el vendedor de cada producto
        ->get()
        ->pluck('product.seller') //Obtener solo los vendedores
        ->unique('id') //Para evitar que vengan campos repetidos
        ->values(); //Reordena la coleccion y limpia campos vacios

        return $this->showAll($sellers);
    }
}
