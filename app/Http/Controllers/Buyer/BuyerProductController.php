<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerProductController extends ApiController
{
    public function __construct(){
        parent::__construct();
        $this->middleware('scope:read-general')->only('index');
    }

    //OBTENER TODOS LOS PRODUCTOS QUE UN COMPRADOR A COMPRADO, INDIFERENTEMENTE DE LA TRANSACCION
    public function index(Buyer $buyer)
    {
        $products = $buyer->transactions()->with('product')->get()->pluck('product');
        /*
            Al colocarle los parentesis al transaction se accede al query builder del modelo
            Con with se indica la relacion con productos
            Con pluck se indica la colecion exactamente sobre la collection que queremos, en este caso productos
            y no obtendremos las transacciones
        */
        return $this->showAll($products);
    }
}
