<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use App\Http\Controllers\ApiController;

class BuyerController extends ApiController
{
    public function __construct(){
        parent::__construct();
        $this->middleware('scope:read-general')->only('show');
    }
    
    public function index(){
        $compradores = Buyer::has('transactions')->get(); //Debemos consultar los usuarios que tengan transacciones
        return $this->showAll($compradores);
    }

    public function show($id){
        $compradores = Buyer::has('transactions')->findOrFail($id);
        return $this->showOne($compradores, 201);
    }
}
