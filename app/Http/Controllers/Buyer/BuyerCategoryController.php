<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerCategoryController extends ApiController
{
    
    public function __construct(){
        parent::__construct();
        $this->middleware('scope:read-general')->only('index');
    }
    
    public function index(Buyer $buyer)
    {
        $categories = $buyer->transactions()->with('product.categories') //Acceder a las categorias de los productos
        ->get()
        ->pluck('product.categories') //Obtener solo las categorias del producto
        ->collapse() //Junta todas las listas y las coloca en 1 sola
        ->unique('id') //Para evitar que vengan campos repetidos
        ->values(); //Reordena la coleccion y limpia campos vacios
        
        return $this->showAll($categories);
    }
}
