<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser{

    private function successResponse($data, $code){
        $response = array(
            'status' => 'success',
            'code'   => $code,
            'data'   => $data
        );
        return response()->json($response, $code);
    }

    protected function errorResponse($message, $code){
        $response = array(
            'status' => 'error',
            'code'   => $code,
            'error'=> $message
        );
        return response()->json($response, $code);
    }

    protected function showAll(Collection $collection, $code = 200){
        //VERIFICAR SI LA COLLECCION POSEA ELEMENTOS O NO
        if ($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $code);
        }

        $transformer = $collection->first()->transformer; //OBTENER EL PRIMER ELEMENTO DE LA COLECCION
        
        $collection = $this->filterData($collection, $transformer);
        $collection = $this->sortData($collection, $transformer);
        $collection = $this->paginate($collection);
        $collection = $this->transformData($collection, $transformer);
        $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $instance, $code = 200){
        $transformer = $instance->transformer;
        $data = $this->transformData($instance, $transformer);
        return response()->json($data, $code);
    }

    protected function showMessage($message, $code = 200){
        $response = array(
            'status' => 'success',
            'code'   => $code,
            'data'   => $message
        );
        return $this->successResponse($response, $code);
    }

    //METODO PARA ORDENAR O FILTRAR LOS DATOS
    protected function sortData(Collection $collection, $transformer){
        
        if (request()->has('sort_by')) {
            $attribute = $transformer::originalAttribute(request()->sort_by);

            //Ordenar la coleccion
            $collection = $collection->sortBy->{$attribute};
        }

        return $collection;
    }

    protected function paginate(Collection $collection){
        //Obtener la pagina actual
        $rules = [
			'per_page' => 'integer|min:2|max:50'
		];
		Validator::validate(request()->all(), $rules);
		$page = LengthAwarePaginator::resolveCurrentPage();
		$perPage = 15;
		if (request()->has('per_page')) {
			$perPage = (int) request()->per_page;
		}
		$results = $collection->slice(($page - 1) * $perPage, $perPage)->values();
		$paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
			'path' => LengthAwarePaginator::resolveCurrentPath(),
		]);
		$paginated->appends(request()->all());
		return $paginated;
    }

    protected function filterData(Collection $collection, $transformer){
        //RECORRER LOS VALORES ENVIADOS POR LA URL
        foreach (request()->query() as $query => $value) {
            //Obtener el atributo original
            $attribute = $transformer::originalAttribute($query);

            //verificar si el atributo y el valor no sean vacios
            if (isset($attribute, $value)) {
                //filtrar la colleccion
                $collection = $collection->where($attribute, $value);
            }
        }

        return $collection;
    }

    protected function transformData($data, $transformer){
        $transformation = fractal($data, new $transformer);

        return $transformation->toArray();
    }

    protected function cacheResponse($data){
        //Obtener la URL ac tual
        $url = request()->url();

        //Obtener los parametros de URL
        $queryParams = request()->query();
        
        //ORDENAR UN ARRAY
        ksort($queryParams);
        
		$queryString = http_build_query($queryParams);
        
        $fullUrl = "{$url}?{$queryString}";
        
        return Cache::remember($fullUrl, 3000/60, function() use($data) {
			return $data;
		});
    }
}
