<?php

namespace App\Transformers;

use App\Transaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Transaction $transaction){
        return [
            'identificador'      => (int)$transaction->id,
            'cantidad'           => (int)$transaction->quantity,
            'comprador'          => (int)$transaction->buyer_id,
            'producto'           => (int)$transaction->product_id,
            'fechaCreacion'      => (string)$transaction->created_at,
            'fechaActualizacion' => (string)$transaction->updated_at,
            'fechaEliminacion'   => isset($transaction->deleted_at) ? (string) $transaction->deleted_at : null,
            'links' =>[
                [
                    'rel'  => 'self',
                    'href' => route('transaction.show', $transaction->id),
                ],
                [
                    'rel'  => 'transaction.categories',
                    'href' => route('transaction.categories.index', $transaction->id),
                ],
                [
                    'rel'  => 'transaction.seller',
                    'href' => route('transaction.sellers.index', $transaction->id),
                ],
                [
                    'rel'  => 'buyer',
                    'href' => route('buyers.show', $transaction->buyer_id),
                ],
                [
                    'rel'  => 'product',
                    'href' => route('products.show', $transaction->product_id),
                ]
            ],
        ];
    }

    public static function originalAttribute($index){
        $attribute = [
            'identificador'      => 'id',
            'cantidad'           => 'quantity',
            'comprador'          => 'buyer_id',
            'producto'           => 'seller_id',
            'fechaCreacion'      => 'created_at',
            'fechaActualizacion' => 'updated_at',
            'fechaEliminacion'   => 'deleted_at',
        ];

        return isset($attribute[$index]) ? $attribute[$index] : null;
    }

    public static function transformedAttribute($index){
        $attribute = [
            'id'         => 'identificador',
            'quantity'   => 'cantidad',
            'buyer_id'   => 'comprador',
            'seller_id'  =>   'producto',
            'created_at' => 'fechaCreacion',
            'updated_at' => 'fechaActualizacion',
            'deleted_at' => 'fechaEliminacion',
        ];

        return isset($attribute[$index]) ? $attribute[$index] : null;
    }
}
