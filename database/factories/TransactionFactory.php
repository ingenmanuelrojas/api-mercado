<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Seller;
use App\Transaction;
use App\User;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    $vendedor  = Seller::has('products')->get()->random(); //Obtener todos los usuarios que tengan al menos 1 producto
    $comprador = User::all()->except($vendedor->id)->random();

    return [
        'quantity'   => $faker->numberBetween(1,3),
        'buyer_id'   => $comprador->id,
        'product_id' => $vendedor->products->random()->id,
    ];
});
