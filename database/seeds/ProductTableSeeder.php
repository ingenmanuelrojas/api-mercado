<?php

use App\Category;
use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Product::class, 300)->create()->each(function($producto){
            $categorias = Category::all()->random(mt_rand(1,5))->pluck('id');
            $producto->categories()->attach($categorias);
        });
    }
}
